import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { inject } from 'mobx-react';
import { asyncLocaleProvider, asyncRouter, nomatch } from '@choerodon/boot';
import {MemberRole} from './global/member-role/MemberRole'

// global 对应目录
// const systemSetting = asyncRouter(() => import('./global/system-setting'));
// const memberRole = asyncRouter(() => import('./global/member-role'));
// const menuSetting = asyncRouter(() => import('./global/menu-setting'));
// const organization = asyncRouter(() => import('./global/organization'));
// const role = asyncRouter(() => import('./global/role'));
// const roleLabel = asyncRouter(() => import('./global/role-label'));
// const rootUser = asyncRouter(() => import('./global/root-user'));
// const dashboardSetting = asyncRouter(() => import('./global/dashboard-setting'));
// const projectType = asyncRouter(() => import('./global/project-type'));

// // organization
// const client = asyncRouter(() => import('./organization/client'));
// const orgRole = asyncRouter(() => import('./organization/role'));
// const ldap = asyncRouter(() => import('./organization/ldap'));
// const passwordPolicy = asyncRouter(() => import('./organization/organization-setting/password-policy'));
// const project = asyncRouter(() => import('./organization/project'));
// const user = asyncRouter(() => import('./organization/user'));
// const organizationSetting = asyncRouter(() => import('./organization/organization-setting'));
// const application = asyncRouter(() => import('./organization/application'));

// // project
// const projectSetting = asyncRouter(() => import('./project/project-setting'));

// // user
// const password = asyncRouter(() => import('./user/password'));
// const organizationInfo = asyncRouter(() => import('./user/organization-info'));
// const projectInfo = asyncRouter(() => import('./user/project-info'));
// const tokenManager = asyncRouter(() => import('./user/token-manager'));
// const userInfo = asyncRouter(() => import('./user/user-info'));
// const permissionInfo = asyncRouter(() => import('./user/permission-info'));

// @inject('AppState')
class IAMIndex extends React.Component {
  render() {
    // const { match, AppState } = this.props;
    // const langauge = AppState.currentLanguage;
    // const IntlProviderAsync = asyncLocaleProvider(langauge, () => import(`../locale/${langauge}`));
    return (
      // <IntlProviderAsync>
        <Switch>
          <Route path={`/member-role`} component={MemberRole} />
          {/* <Route path={`/menu-setting`} component={menuSetting} />
          <Route path={`/system-setting`} component={systemSetting} />
          <Route path={`/organization`} component={organization} />
          <Route path={`/role`} component={role} />
          <Route path={`/org-role`} component={orgRole} />
          <Route path={`/role-label`} component={roleLabel} />
          <Route path={`/root-user`} component={rootUser} />
          <Route path={`/dashboard-setting`} component={dashboardSetting} />
          <Route path={`/client`} component={client} />
          <Route path={`/ldap`} component={ldap} />
          <Route path={`/password-policy`} component={passwordPolicy} />
          <Route path={`/project`} component={project} />
          <Route path={`/user`} component={user} />
          <Route path={`/project-setting`} component={projectSetting} />
          <Route path={`/password`} component={password} />
          <Route path={`/organization-info`} component={organizationInfo} />
          <Route path={`/project-info`} component={projectInfo} />
          <Route path={`/token-manager`} component={tokenManager} />
          <Route path={`/user-info`} component={userInfo} />
          <Route path={`/permission-info`} component={permissionInfo} />
          <Route path={`/organization-setting`} component={organizationSetting} />
          <Route path={`/project-type`} component={projectType} />
          <Route path={`/application`} component={application} /> */}
          {/* <Route path="*" component={nomatch} /> */}
        </Switch>
      //  </IntlProviderAsync>
    );
  }
}

export default IAMIndex;
